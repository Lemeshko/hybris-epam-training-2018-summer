package by.training.controllers;

import by.training.data.UserData;
import by.training.ifaces.facades.UserFacade;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class UserController {
    private UserFacade userFacade;

    @RequestMapping("/users")
    public String showUsers(final Model model) {
        final List<UserData> users = userFacade.getUsers();
        model.addAttribute("users", users);
        return "usersList";
    }

    @Required
    @Resource(name = "userFacade")
    public void setUserFacade(UserFacade userFacade) {
        this.userFacade = userFacade;
    }
}
