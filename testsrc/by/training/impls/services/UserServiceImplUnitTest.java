package by.training.impls.services;

import by.training.ifaces.dao.UserDAO;
import by.training.ifaces.services.UserService;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceImplUnitTest {
    private final String uid = "007";
    private UserServiceImpl userService;
    private UserDAO userDAO;
    private UserModel userModel;

    @Before
    public void setUp() throws Exception {
        userService = new UserServiceImpl();
        userDAO = mock(UserDAO.class);
        userService.setUserDAO(userDAO);
        userModel = new UserModel();
        userModel.setUid(uid);
    }

    @Test
    public void testGetUsers() {
        final List<UserModel> userModels = Arrays.asList(userModel);
        when(userDAO.getUsers()).thenReturn(userModels);
        final List<UserModel> users = userService.getUsers();
        assertEquals("We should find one", 1, users.size());
        assertEquals("And should equals what the mock returned", userModel, users.get(0));

    }
}