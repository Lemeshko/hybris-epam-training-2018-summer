package by.training.impls.services;

import by.training.ifaces.services.UserService;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;

@IntegrationTest
public class UserServiceImplIntegrationTest extends ServicelayerTransactionalTest {
    private final String uid = "007";

    @Resource
    private ModelService modelService;

    @Resource(name = "userServiceIml")
    private UserService userService;

    private UserModel model;

    @Before
    public void setUp() {
        model = new UserModel();
        model.setUid(uid);
    }

    @Test
    public void userServiceTest() {
        List<UserModel> users = userService.getUsers();
        final int size = users.size();

        modelService.save(model);

        users = userService.getUsers();
        assertEquals(size + 1, users.size());
    }

}