package by.training.impls.facades;

import by.training.data.UserData;
import by.training.ifaces.services.UserService;
import de.hybris.platform.core.model.user.UserModel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserFacadeImplUnitTest {
    private final String uid = "007";
    private final String name = "Bond";
    @Mock
    private UserService userService;
    @InjectMocks
    private UserFacadeImpl userFacade = new UserFacadeImpl();

    private List<UserModel> dummyDataUsers() {
        final UserModel userModel = new UserModel();
        userModel.setUid(uid);
        userModel.setName(name);
        final List<UserModel> userModels = new ArrayList<>();
        userModels.add(userModel);
        return userModels;
    }

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getUsers() {
        final List<UserModel> userModels = dummyDataUsers();
        final UserModel userModel = new UserModel();
        userModel.setUid(uid);
        userModel.setName(name);
        when(userService.getUsers()).thenReturn(userModels);

        final List<UserData> userData = userFacade.getUsers();
        assertNotNull(userData);
        assertEquals(userModels.size(), userData.size());
        assertEquals(userModel.getName(), userData.get(0).getName());
    }
}