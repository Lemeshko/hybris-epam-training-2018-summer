package by.training.impls.facades;

import by.training.data.UserData;
import by.training.ifaces.facades.UserFacade;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@IntegrationTest
public class UserFacadeImplIntegrationTest extends ServicelayerTransactionalTest {
    private final String uid = "007";
    @Resource
    private UserFacade userFacade;

    @Resource
    private ModelService modelService;

    private UserModel userModel;

    @Before
    public void setUp() throws Exception {
        userModel = new UserModel();
        userModel.setUid(uid);
    }

    @Test
    public void getUsers() {
        List<UserData> userData = userFacade.getUsers();
        assertNotNull(userData);

        final int size = userData.size();
        modelService.save(userModel);

        userData = userFacade.getUsers();
        assertNotNull(userData);
        assertEquals(size + 1, userData.size());
    }
}