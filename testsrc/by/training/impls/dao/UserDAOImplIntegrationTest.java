package by.training.impls.dao;

import by.training.ifaces.dao.UserDAO;
import de.hybris.bootstrap.annotations.IntegrationTest;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.ServicelayerTransactionalTest;
import de.hybris.platform.servicelayer.model.ModelService;
import org.junit.Test;

import javax.annotation.Resource;
import java.util.List;

import static org.junit.Assert.assertEquals;

@IntegrationTest
public class UserDAOImplIntegrationTest extends ServicelayerTransactionalTest {
    private final String uid = "007";
    @Resource
    private UserDAO userDAO;

    @Resource
    private ModelService modelService;

    @Test
    public void userDAOTest() {
        List<UserModel> users = userDAO.getUsers();
        final int size = users.size();

        UserModel model = new UserModel();
        model.setUid(uid);
        modelService.save(model);

        users = userDAO.getUsers();
        assertEquals(size + 1, users.size());
    }
}