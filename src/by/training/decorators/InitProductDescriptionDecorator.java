package by.training.decorators;

import de.hybris.platform.util.CSVCellDecorator;

import java.util.Map;

public class InitProductDescriptionDecorator implements CSVCellDecorator {
    @Override
    public String decorate(int i, Map<Integer, String> map) {
        String srcLine = map.get(i);
        return srcLine + "_customizedDuringInitialization";
    }
}
