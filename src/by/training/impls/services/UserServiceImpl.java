package by.training.impls.services;


import by.training.ifaces.dao.UserDAO;
import by.training.ifaces.services.UserService;
import de.hybris.platform.core.model.user.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service(value = "userServiceIml")
public class UserServiceImpl implements UserService {
    private UserDAO userDAO;

    @Override
    public List<UserModel> getUsers() {
        return userDAO.getUsers();
    }

    @Required
    @Resource(name = "userDAO")
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
}
