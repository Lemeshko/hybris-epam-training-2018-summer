package by.training.impls.dao;

import by.training.ifaces.dao.UserDAO;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(value = "userDAO")
public class UserDAOImpl implements UserDAO {
    @Autowired
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<UserModel> getUsers() {
        final String queryString = "SELECT {p:" + UserModel.PK + "} "
                + "FROM {" + UserModel._TYPECODE + " AS p} ";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        return flexibleSearchService.<UserModel>search(query).getResult();
    }
}
