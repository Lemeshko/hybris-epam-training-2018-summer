package by.training.impls.dao;

import by.training.ifaces.dao.ProductDAO;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.search.FlexibleSearchQuery;
import de.hybris.platform.servicelayer.search.FlexibleSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(value = "productDAO")
public class ProductDAOImpl implements ProductDAO {
    @Autowired
    private FlexibleSearchService flexibleSearchService;

    @Override
    public List<ProductModel> getProducts() {
        final String queryString = "SELECT {p:" + ProductModel.PK + "} "
                + "FROM {" + ProductModel._TYPECODE + " AS p} ";
        final FlexibleSearchQuery query = new FlexibleSearchQuery(queryString);
        return flexibleSearchService.<ProductModel>search(query).getResult();
    }
}
