package by.training.impls.facades;

import by.training.data.UserData;
import by.training.ifaces.facades.UserFacade;
import by.training.ifaces.services.UserService;
import de.hybris.platform.core.model.user.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "userFacade")
public class UserFacadeImpl implements UserFacade {
    @Autowired
    private UserService userServiceImpl;

    @Override
    public List<UserData> getUsers() {
        final List<UserModel> userModels = userServiceImpl.getUsers();
        final List<UserData> dataList = new ArrayList<>();
        for (final UserModel model : userModels) {
            final UserData userData = new UserData();
            userData.setName(model.getName());
            dataList.add(userData);
        }
        return dataList;
    }

}
