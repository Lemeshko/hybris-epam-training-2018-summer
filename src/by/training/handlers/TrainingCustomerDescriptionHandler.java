package by.training.handlers;

import com.google.common.base.Preconditions;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.servicelayer.model.attribute.DynamicAttributeHandler;

public class TrainingCustomerDescriptionHandler implements DynamicAttributeHandler<String, CustomerModel> {
    @Override
    public String get(CustomerModel model) {
        Preconditions.checkNotNull(model, "Item model is required");
        return String.format("%1$s : %2$s. Order quantity is %3$s",
                model.getName(), model.getEmail(), model.getOrders().size());
    }

    @Override
    public void set(CustomerModel model, String s) {
        throw new UnsupportedOperationException();
    }
}
