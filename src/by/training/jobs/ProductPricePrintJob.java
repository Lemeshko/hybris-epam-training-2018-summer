package by.training.jobs;

import by.training.ifaces.dao.ProductDAO;
import by.training.model.ProductPricePrintCronJobModel;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.cronjob.enums.CronJobResult;
import de.hybris.platform.cronjob.enums.CronJobStatus;
import de.hybris.platform.servicelayer.cronjob.AbstractJobPerformable;
import de.hybris.platform.servicelayer.cronjob.PerformResult;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProductPricePrintJob extends AbstractJobPerformable<ProductPricePrintCronJobModel> {
    static final Logger LOGGER = Logger.getLogger(ProductPricePrintJob.class);
    @Autowired
    private ProductDAO productDAO;

    @Override
    public PerformResult perform(ProductPricePrintCronJobModel productPricePrintCronJobModel) {
        final List<ProductModel> products = productDAO.getProducts();
        LOGGER.info("Start job");
        for (ProductModel model : products) {
            LOGGER.info(String.format("Product %1$s amount price ---> %2$s ",model.getName(), model.getPriceQuantity()));
        }
        LOGGER.info("End job");
        return new PerformResult(CronJobResult.SUCCESS, CronJobStatus.FINISHED);
    }
}
