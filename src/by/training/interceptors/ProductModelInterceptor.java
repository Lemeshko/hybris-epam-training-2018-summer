package by.training.interceptors;

import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.interceptor.InterceptorContext;
import de.hybris.platform.servicelayer.interceptor.InterceptorException;
import de.hybris.platform.servicelayer.interceptor.ValidateInterceptor;
import org.apache.log4j.Logger;


public class ProductModelInterceptor implements ValidateInterceptor<ProductModel> {
    static final Logger LOGGER = Logger.getLogger(ProductModelInterceptor.class);

    @Override
    public void onValidate(ProductModel productModel, InterceptorContext interceptorContext) throws InterceptorException {
        String description = productModel.getDescription();
        if (description.contains("_")) {
            LOGGER.info(String.format("Product model %1$s contains _  : %2$s", productModel.getName(), description));
        }
    }
}
