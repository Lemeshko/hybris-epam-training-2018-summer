package by.training.setups;

import by.training.constants.TrainingConstants;
import de.hybris.platform.core.initialization.SystemSetup;
import de.hybris.platform.impex.jalo.ImpExException;
import de.hybris.platform.impex.jalo.ImpExManager;
import de.hybris.platform.impex.jalo.Importer;
import de.hybris.platform.util.CSVReader;
import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;


@SystemSetup(extension = TrainingConstants.EXTENSIONNAME)
public class DataSystemSetup {
    static final Logger LOGGER = Logger.getLogger(DataSystemSetup.class);

    @SystemSetup(extension = TrainingConstants.EXTENSIONNAME, process = SystemSetup.Process.UPDATE)
    public void updateDescriptionDuringUpdateProcess() {
        String updateImpex = ImpExManager.class.getResource(
                "/training/import/custom-update-description.impex").getPath();
        importAll(updateImpex);
    }

    @SystemSetup(extension = TrainingConstants.EXTENSIONNAME, process = SystemSetup.Process.INIT)
    public void updateDescriptionDuringInitProcess() {
        String initImpex = ImpExManager.class.getResource(
                "/training/import/custom-init-description.impex").getPath();
        importAll(initImpex);
    }

    private void importAll(String fileName) {
        CSVReader reader = null;
        try {
            reader = new CSVReader(fileName, "utf-8");
            Importer importer = new Importer(reader);
            importer.importAll();
        } catch (UnsupportedEncodingException e) {
            LOGGER.fatal("Given encoding is not supported", e);
        } catch (FileNotFoundException e) {
            LOGGER.fatal("Given file is not found", e);
        } catch (ImpExException e) {
            LOGGER.fatal("Fail importAll from importer instance", e);
        }
    }
}
