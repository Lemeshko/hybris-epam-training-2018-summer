package by.training.listeners;

import de.hybris.platform.core.PK;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.servicelayer.model.ModelService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.tx.AfterSaveEvent;
import de.hybris.platform.tx.AfterSaveListener;
import de.hybris.platform.workflow.WorkflowProcessingService;
import de.hybris.platform.workflow.WorkflowService;
import de.hybris.platform.workflow.WorkflowTemplateService;
import de.hybris.platform.workflow.model.WorkflowActionModel;
import de.hybris.platform.workflow.model.WorkflowModel;
import de.hybris.platform.workflow.model.WorkflowTemplateModel;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;

public class ProductChangeListener implements AfterSaveListener {
    static final Logger LOGGER = Logger.getLogger(ProductChangeListener.class);
    private final int productDeploymentCode = 1;
    private final String productUpdateWorkflowCode = "PU";
    @Autowired
    private WorkflowService workflowService;
    @Autowired
    private WorkflowTemplateService workflowTemplateService;
    @Autowired
    private WorkflowProcessingService workflowProcessingService;
    @Autowired
    private UserService userService;
    @Autowired
    private ModelService modelService;

    @Override
    public void afterSave(Collection<AfterSaveEvent> collection) {
        for (final AfterSaveEvent event : collection) {
            final int type = event.getType();
            if (AfterSaveEvent.UPDATE == type) {
                final PK pk = event.getPk();
                if (productDeploymentCode == pk.getTypeCode()) {
                    final ProductModel model = modelService.get(pk);
                    final WorkflowTemplateModel template = this.workflowTemplateService
                            .getWorkflowTemplateForCode(productUpdateWorkflowCode);

                    final WorkflowModel workflow = this.workflowService.
                            createWorkflow(template, model, userService.getAdminUser());
                    modelService.save(workflow);
                    for (final WorkflowActionModel action : workflow.getActions()) {
                        modelService.save(action);
                    }
                    this.workflowProcessingService.startWorkflow(workflow);
                    LOGGER.info(String.format("Start PruductChangeListener, product name: %1$s , product pk typeCode: %2$s"
                            , model.getName(), model.getPk().getTypeCode()));
                }
            }
        }
    }
}
