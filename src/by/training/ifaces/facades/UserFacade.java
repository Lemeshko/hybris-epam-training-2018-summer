package by.training.ifaces.facades;

import by.training.data.UserData;

import java.util.List;

public interface UserFacade {

    List<UserData> getUsers();
}
