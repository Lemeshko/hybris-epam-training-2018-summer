package by.training.ifaces.services;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

public interface UserService {

    List<UserModel> getUsers();

}
