package by.training.ifaces.dao;

import de.hybris.platform.core.model.product.ProductModel;

import java.util.List;

public interface ProductDAO {

    List<ProductModel> getProducts();

}
