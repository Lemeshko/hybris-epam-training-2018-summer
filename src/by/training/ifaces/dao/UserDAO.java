package by.training.ifaces.dao;

import de.hybris.platform.core.model.user.UserModel;

import java.util.List;

public interface UserDAO {

    List<UserModel> getUsers();
}
